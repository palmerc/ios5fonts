//
//  CPAppDelegate.h
//  iOS5Fonts
//
//  Created by Cameron Lowell Palmer on 15.10.12.
//  Copyright (c) 2012 Cameron Lowell Palmer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
